const fs = require("fs");
const path = require("path");
const file1Path = path.join(__dirname, "file1.txt");
const file2Path = path.join(__dirname, "file2.txt");
const file3Path = path.join(__dirname, "file3.txt");
const lipsum = path.join(__dirname, "lipsum.txt");



// Solution 1

function writeFilePromise(filePath, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, data, (err) => {
      if (err) {
        reject('error happened while creating a file');
      } else {
        resolve("created file");
      }
    });
  });
}

function deleteFilePromise(filePath) {
  return new Promise((resolve, reject) => {
    fs.unlink(filePath, (err) => {
      if (err) {
        reject('error happened while deleting a file');
      } else {
        resolve(`deleted ${filePath} `);
      }
    });
  });
}

function createAndDeleteFiles() {

  const createFile1Promise = writeFilePromise(file1Path, "Hello World!");
  const createFile2Promise = writeFilePromise(file2Path, "Good Morning");

  return Promise.all([createFile1Promise, createFile2Promise])
    .then(() => {
      console.log("created files");

      return new Promise((resolve) => {
        setTimeout(() => {
          resolve();
        }, 2 * 1000);
      });

    })
    .then(() => {
      return deleteFilePromise(file1Path);

    })
    .then((res) => {
      console.log(res);
      return deleteFilePromise(file2Path);

    })
    .then((res) => {
      console.log(res);

    })
    .catch((err) => {
        console.log(err);
    
      })
      
}

createAndDeleteFiles();



// SOlution 2



function readFile(filePath){

    return new Promise((resolve, reject) => {
        fs.readFile(filePath, "utf8", (err, data) => {
          if (err) {
            reject('error happened while creating a file');
          } else {
            resolve(data);
          }
        });
      });
    }



function readWriteAndDeleteFile(){

return readFile(lipsum).then((data) => {
    console.log(data);
    return writeFilePromise(file3Path, data)
}).then((res) => {
    console.log(res);
    return deleteFilePromise(lipsum);
}).then((res) => {
    console.log(res);
}).catch((err) => {
    console.log(err);
})

}

readWriteAndDeleteFile();